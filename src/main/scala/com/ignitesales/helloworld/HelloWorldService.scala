package com.ignitesales.helloworld

import cats.effect.Effect
import com.ignitesales.dep1.Constants.Greeting
import com.ignitesales.dep2.Constants.MsgKey
import io.circe.Json
import org.http4s.HttpService
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

class HelloWorldService[F[_]: Effect] extends Http4sDsl[F] {

  val service: HttpService[F] = {
    HttpService[F] {
      case GET -> Root / "hello" / name =>
        Ok(Json.obj(MsgKey -> Json.fromString(s"${Greeting}, ${name}")))
    }
  }
}
